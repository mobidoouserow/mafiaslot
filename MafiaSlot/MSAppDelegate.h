//
//  AppDelegate.h
//  MafiaSlot
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

