//
//  MSAppDelegate.m
//  MafiaSlot
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "MSAppDelegate.h"

#import "MafiaSlotViewController.h"

#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface MSAppDelegate ()

@property (strong, nonatomic) UIViewController *viewController;

@end

@implementation MSAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.viewController = [[MafiaSlotViewController alloc] init];
    
    self.window.rootViewController = self.viewController;
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    [self.window makeKeyAndVisible];
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - YandexMobileMetrica

+ (void)initialize
{
    if ([self class] == [MSAppDelegate class]) {
        /* Replace API_KEY with your unique API key. Please, read official documentation how to obtain one:
         https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/tasks/ios-quickstart-docpage/
         */
        [YMMYandexMetrica activateWithApiKey:@"65bf04ed-ae25-47c1-8c0a-2cbb372f5bc6"];
        //manual log setting for whole library
        [YMMYandexMetrica setLoggingEnabled:YES];
    }
}

@end
