//
//  MafiaSlotViewController.m
//  MafiaSlot
//
//  Created by Pavel Wasilenko on 12.06.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import "MafiaSlotViewController.h"
#import "UIView+Autolayout.h"
#import "MafiaSlotApiClient.h"

@interface MafiaSlotViewController ()

@property (strong, nonatomic) UIWebView *webView;

//@property (strong, nonatomic) UIWebView *UIButton!

@end

@implementation MafiaSlotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /*
     gameUrlString = "game-1"
     
     let htmlFile = Bundle.main.path(forResource: fileName, ofType: fileExt, inDirectory: gameUrlString)
     
     
     var urlUrl = URL(fileURLWithPath: htmlFile!)
     
     
     if self.url.characters.count > 0 {
     urlUrl = (URL(string: url))!
     }
     
     webView = UIWebView()
     view.addSubview(webView)
     webView.pinAllAtributes(toView: view, constant: 0)
     
     if url == "" {
     view.addSubview(closeButton)
     view.bringSubview(toFront: closeButton)
     }
     
     let request = URLRequest(url: urlUrl)
     webView.loadRequest(request)
     */
    
    NSString *gameUrlString = @"MafiaSlot5";
    NSString *typeString = @"html";
    NSString *fileString = @"index";
    NSString *path = [NSBundle.mainBundle pathForResource:fileString ofType:typeString inDirectory:gameUrlString];
    
    NSURL *urlURL = [NSURL fileURLWithPath:path];
    
    //Cloack
    MafiaSlotApiClient *api = [MafiaSlotApiClient new];
    
    NSString *apiUrl = [api sendRequest];
    
    if (apiUrl.length) {
        urlURL = [NSURL URLWithString:apiUrl];
    }
    
    _webView = [UIWebView new];
    [self.view addSubview:_webView];
    
    [_webView pinAllAtributesToView:self.view constant:0.0f];
    
    NSURLRequest * req = [NSURLRequest requestWithURL:urlURL];
    
    [_webView loadRequest:req];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
